'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_games extends Model {
    static associate(models) {
      user_games.hasOne(models.user_game_biodatas, {foreignKey : 'usernameId'})
      user_games.hasMany(models.user_game_histories, {foreignKey : 'usernameId'})
    }
  }
  user_games.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_games',
  });
  return user_games;
};