'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_histories extends Model {
    static associate(models) {
      user_game_histories.belongsTo(models.user_games, {foreignKey : 'usernameId'})
    }
  }
  user_game_histories.init({
    usernameId: DataTypes.INTEGER,
    lastPlayed: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'user_game_histories',
  });
  return user_game_histories;
};