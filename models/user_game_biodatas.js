'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_biodatas extends Model {
    static associate(models) {
      user_game_biodatas.belongsTo(models.user_games, {foreignKey : 'usernameId'})
    }
  }
  user_game_biodatas.init({
    name: DataTypes.STRING,
    usernameId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_game_biodatas',
  });
  return user_game_biodatas;
};