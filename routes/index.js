const ViewController = require('../controller/viewController')
const ModelController = require('../controller/modelController')
const route = require('express').Router()


route.get('/', ViewController.home)
route.get('/register', ViewController.register)
route.post('/register', ModelController.register)
route.get('/delete/:id', ModelController.delete)
route.get('/edit/:id', ViewController.edit)
route.post('/save-edit/:id', ModelController.edit)
route.get('/login', ViewController.login)
route.post('/login', ModelController.login)
route.get('/biodata', ViewController.biodata)
route.get('/history', ViewController.history)


module.exports = route