const {user_games,user_game_biodatas} = require('../models')
// const user_game_biodatas = require('../models/user_game_biodatas')

module.exports = class ModelController {
    static async register (req,res) {
        try {
            const { username, password, role } = req.body
            const payload = { username, password, role }
            await user_games.create(payload)
            res.redirect('/')
        } catch (error) {
            res.send('error')
        }
    }

    static async delete (req,res) {
        try {
            const id = req.params.id
            let deleteData = await user_games.destroy({
                where : {
                    id
                }
            })
            res.redirect('/')
        } catch (error) {
            res.send('error')
        }
    }

    static async edit (req,res) {
        try {
            const id = req.params.id
            const {username, password, role} = req.body
            const payload = {username, password, role}
            const data = await user_games.update(payload, {
                where : {
                    id
                }
            })
            res.redirect('/')
        } catch (error) {
            res.send('error')
        }
    }

    static async login (req,res) {
        try {
            const {username, password} = req.body
            
            let usernameLogin = await user_games.findOne({
                where : {
                    username 
                }
            })

            if(usernameLogin) {
                usernameLogin = usernameLogin.toJSON()
                const role = usernameLogin.role

                if(password !== usernameLogin.password) {
                    return res.redirect('/login')
                }else{
                    if(role !== 'super user') {
                        res.redirect('/login')
                    }else{
                        res.redirect('/')
                    }
                }
            }else{
                res.redirect('/login')
            }
        } catch (error) {
            res.send('error')
        }
        
    }
}